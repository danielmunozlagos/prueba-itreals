<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\persona;
use App\Models\persona_telefono;
use App\Jobs\modificarInsert;

class personaController extends Controller
{
	public function __construct(){
    	
		$this->validate = [
			'rules' => [
				'nombres' => 'required',
				'ap_paterno' => 'required',
				'ap_materno' => 'required',
				'run' => 'required',
				'telefono' => 'required',
			],'message'=> [
				'nombres.required' => 'Debes ingresar el o los nombres',
				'ap_paterno.required' => 'Debes ingresar apellido paterno',
				'ap_materno.required' => 'Debes ingresar apellido materno',
				'run.required' => 'Debes ingresar run',
				'telefono.required' => 'Debes ingresar telefono',
			]
		];		
    }

    public function show(){
		return view('ingreso');
	}

	public function create(Request $request){
		$this->validate($request, $this->validate['rules'], $this->validate['message'] );
		
		$persona = new persona;
		$persona->nombres = $request->nombres;
		$persona->ap_paterno = $request->ap_paterno;
		$persona->ap_materno = $request->ap_materno;
		$persona->run = $request->run;
		
		if($persona->save()){
			$telefono = new persona_telefono;
			$telefono->id_persona = $persona->id;
			$telefono->telefono = $request->telefono;
			$telefono->save();
		}

		$registro = persona_telefono::find($telefono->id);
		$job = new \App\Jobs\modificaInsert();
		dispatch($job);
		return view('resultado', compact('registro'));

	}
}
