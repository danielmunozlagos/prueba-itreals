<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class persona_telefono extends Model
{
    protected $table = 'persona_telefono';
	protected $fillable = ['id_persona', 'telefono'];
	public $timestamps = true;
	protected $with = [ 'persona_telefono' ];

	// Relaciones
	public function persona_telefono(){
		return $this->belongsTo('App\Models\persona', 'id_persona');
	}
}
