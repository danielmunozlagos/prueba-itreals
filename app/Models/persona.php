<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class persona extends Model
{
    protected $table = 'persona';
	protected $fillable = ['run', 'nombres', 'ap_paterno', 'ap_materno'];
	public $timestamps = true;

}
