@extends('welcome')

@section('content')
<div class="container center">
	<div class="jumbotron">
		<h1>Formulario</h1>
	</div>
</div>

<form method="POST">
	@csrf
	<div class="form-group row">
		<label for="inputNombres" class="col-sm-2 col-form-label">Nombres</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="inputNombres" name ="nombres" placeholder="Nombres" required>
		</div>
	</div>
	<div class="form-group row">
		<label for="inputApPaterno" class="col-sm-2 col-form-label">Apellido Paterno</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="inputApPaterno" name="ap_paterno" placeholder="Apellido Paterno" required>
		</div>
	</div>
	<div class="form-group row">
		<label for="inputApMaterno" class="col-sm-2 col-form-label">Apellido Materno</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="inputApMaterno" name="ap_materno" placeholder="Apellido Materno" required>
		</div>
	</div>
	<div class="form-group row">
		<label for="inputRun" class="col-sm-2 col-form-label">Run</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="inputRun" oninput="checkRut(this)" name="run" placeholder="12345678-9" required>
		</div>
	</div>
	<div class="form-group row">
		<label for="inputTelefono" class="col-sm-2 col-form-label">Telefono</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="inputTelefono" name="telefono" placeholder="56 9 1234 5678" required>
		</div>
	</div>
	<div class="form-group row">
		<div class="col-sm-10">
			<button type="submit" class="btn btn-primary">Guardar</button>
		</div>
	</div>
</form>

@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif


<script type="text/javascript">
	function checkRut(rut) {
	    // Despejar Puntos
	    var valor = rut.value.replace('.','');
	    // Despejar Guión
	    valor = valor.replace('-','');
	    
	    // Aislar Cuerpo y Dígito Verificador
	    cuerpo = valor.slice(0,-1);
	    dv = valor.slice(-1).toUpperCase();
	    
	    // Formatear RUN
	    rut.value = cuerpo + '-'+ dv
	    
	    // Si no cumple con el mínimo ej. (n.nnn.nnn)
	    if(cuerpo.length < 7) { rut.setCustomValidity("RUT Incompleto"); return false;}
	    
	    // Calcular Dígito Verificador
	    suma = 0;
	    multiplo = 2;
	    
	    // Para cada dígito del Cuerpo
	    for(i=1;i<=cuerpo.length;i++) {
	    
	        // Obtener su Producto con el Múltiplo Correspondiente
	        index = multiplo * valor.charAt(cuerpo.length - i);
	        
	        // Sumar al Contador General
	        suma = suma + index;
	        
	        // Consolidar Múltiplo dentro del rango [2,7]
	        if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
	  
	    }
	    
	    // Calcular Dígito Verificador en base al Módulo 11
	    dvEsperado = 11 - (suma % 11);
	    
	    // Casos Especiales (0 y K)
	    dv = (dv == 'K')?10:dv;
	    dv = (dv == 0)?11:dv;
	    
	    // Validar que el Cuerpo coincide con su Dígito Verificador
	    if(dvEsperado != dv) { rut.setCustomValidity("RUT Inválido"); return false; }
	    
	    // Si todo sale bien, eliminar errores (decretar que es válido)
	    rut.setCustomValidity('');
	}
</script>
@endsection