@extends('welcome')

@section('content')

<div class="container center">
	<div class="jumbotron">
		<h1>SE REGISTRO CORRECTAMENTE</h1>
	</div>

	@isset($registro)
	<ul class="list-group">
		<li class="list-group-item">{!! $registro->persona_telefono->nombres !!}</li>
		<li class="list-group-item">{!! $registro->persona_telefono->ap_paterno !!}</li>
		<li class="list-group-item">{!! $registro->persona_telefono->ap_materno !!}</li>
		<li class="list-group-item">{!! $registro->persona_telefono->run !!}</li>
		<li class="list-group-item">{!! $registro->telefono !!}</li>
		<li class="list-group-item">{!! date('d-m-Y', strtotime($registro->persona_telefono->created_at)) !!}</li>
	</ul>
	@endisset
	<br>
	<a href="/" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Inicio</a>
</div>

@endsection